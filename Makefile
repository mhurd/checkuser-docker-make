SHELL := /bin/bash

.DEFAULT: freshinstallwithwebextensions
.PHONY: freshinstallwithwebextensions
freshinstallwithwebextensions:
	-@if [ ! -f "./mediawiki-docker-make/Makefile" ]; then \
		git submodule update --init; \
	fi
#	The include file should now be in place, so subsequent "make" calls will have access to the include vars
	make continueinstallation;

appendfilecontentstolocalsettings:
	cat ./extralocalsettings.txt >> "./mediawiki-docker-make/mediawiki/LocalSettings.php";

investigate_url = "http://localhost:$(mediawiki_port)/w/index.php?title=Special:UserLogin&returnto=Special:Investigate"

.PHONY: continueinstallation
continueinstallation:
	make freshinstall skipopenspecialversionpage=true;
	make appendfilecontentstolocalsettings;
	make useminervaneueskin skipopenspecialversionpage=true;
	set -k; ./mediawiki-docker-make/utility.sh apply_mediawiki_extension mediawikiPath=$(mediawiki_dir) extensionRepoURL=https://gerrit.wikimedia.org/r/mediawiki/extensions/IPInfo extensionBranch=master extensionSubdirectory=IPInfo wfLoadExtension=IPInfo;
	set -k; ./mediawiki-docker-make/utility.sh apply_mediawiki_extension mediawikiPath=$(mediawiki_dir) extensionRepoURL=https://gerrit.wikimedia.org/r/mediawiki/extensions/CheckUser extensionBranch=master extensionSubdirectory=CheckUser wfLoadExtension=CheckUser;
	cd $(mediawiki_dir) && docker compose exec mediawiki /bin/bash -c "cd extensions/CheckUser && composer update --no-dev; php /var/www/html/w/maintenance/update.php"
	@echo -e "\nOpening '$(investigate_url)'.\nLog in using 'Admin' and 'dockerpass' if needed.\n";
	$(makefile_dir)/utility.sh open_url_when_available $(investigate_url);

-include ./mediawiki-docker-make/Makefile